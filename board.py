# klasa plansza ma posiadać pustą plansze w init, ale nie mam jej podawać jako argument poniewaz nie chce wywolywac argumentu przy wywolaniu funkcji
class Board:
    def __init__(self):
        self.board = ["-" for _ in range(8) for _ in range(8)] # przechowuje aktualny stan planszy
        self.white_pawns = [[7,0],[7,2],[7,4],[7,6],[6,1],[6,3],[6,5],[6,7],[5,0],[5,2],[5,4],[5,6]]
        self.black_pawns = [[0,1],[0,3],[0,5],[0,7],[1,0],[1,2],[1,4],[1,6],[2,1],[2,3],[2,5],[2,7]]

# metoda, która wyświetla plansze do gry
    def display_board(self):
        print('  A B C D E F G H')
        for row in range(8):
            print(row + 1, end=" ")
            for field in range(8):
                print(self.board[field + row*8], end=" ")
            print(row + 1)
        print('  A B C D E F G H')

# metoda, która dodaje pionki i ustatawia je w pozycji startowej
    def add_pawns_to_board(self):
        for position in self.white_pawns:
            row, col = position
            self.board[row*8 + col] = 'W' # Obliczanie indeksu wiersza i kolumny, aby dobrze przypisać pionka
        for position in self.black_pawns:
            row, col = position
            self.board[row*8 + col] = 'B'
        self.display_board()
        

# Klasa odpowiedzialna za poruszanie się
class Move(Board):
    def __init__(self):
        super().__init__()
        self.mapped_pawns_white = []
        self.mapped_pawns_black = []

# metoda która mapuje koordynaty pionków 
    def map_position(self):
        # mapowanie row na zakres od 1 do 8
        map_row_white = map(lambda row: (row[0] + 1, row[1]), self.white_pawns)
        self.white_pawns = list(map_row_white)
        map_row_black = map(lambda row: (row[0] + 1, row[1]), self.black_pawns)
        self.black_pawns = list(map_row_black)

        # mapowanie na pionkach. Nie na całej planszy self.board 
        # mapowanie column na zakres od A do H
        # stworzone 2 listy ktore zapisja zmapowane koordynaty pionkow
        col_map = {0:'A', 1:'B', 2:'C', 3:'D', 4:'E', 5:'F', 6:'G', 7:'H'} #uzyć do zmapowania col 
        mapped_pawns_white = [] # zmapowane kordynaty pionkow bialych
        mapped_pawns_black = [] # zmapowane kordynaty pionkow czarnych

        for pawn in self.white_pawns: #iteracja po liscie 
            row, col = pawn
            col_letter = col_map[col] # przypisanie zmiennej do dictionary 
            mapped_pawns_white.append([row, col_letter])
        

        for pawn in self.black_pawns:
            row, col = pawn
            col_letter = col_map[col]
            mapped_pawns_black.append([row, col_letter])
        

        self.mapped_pawns_white = mapped_pawns_white
        self.mapped_pawns_black = mapped_pawns_black

        
    def move_pawn_white(self):
        self.white_turn = True
        beaten_pawns_black = []
        # Pętla, ktora wymienia ruchy pomiedzy graczami
        while self.white_turn == True:
            # Sprawdzenie przymusowego bicia pionka przeciwnika
            while True:
                for pawn_white in self.mapped_pawns_white:
                    for pawn_black in self.mapped_pawns_black:
    
                        white_row = pawn_white[0]
                        white_col = ord(pawn_white[1]) - ord("A")
                        white_index = (int(white_row - 1) * 8 + white_col)

                        black_row = pawn_black[0] # przypisanie numeru wiersza pionka czarnego do zmiennej
                        black_col = ord(pawn_black[1]) - ord("A") # przypisanie i zamiana kolumny pionka czarnego na liczbe
                        black_index = (int(black_row - 1) * 8 + black_col)
                        
                        if black_row == 8:
                            print("Doszedles do konca")
                        elif black_col >= 1 and self.board[black_index + 9] == "W" and self.board[black_index - 9] == "-":  
                # tutaj cały czas pracuje na self.mapped pawns white 
                            if white_col >= 2 and self.board[white_index - 9] == "B":
                                if self.board[white_index - 18] == "-":

                                    while True:
                                        player_white = input("Mandatory jump! Which pawn do you want to move? lewo: ").upper()
                                        pawn_index_white = self.mapped_pawns_white.index([int(player_white[0]), player_white[1]]) # zapisanie indeksu w self.mapped_pawns_w
                                        pawn_index_black = self.mapped_pawns_black.index([pawn_black[0], pawn_black[1]])
                                        row, col = int(player_white[0]) - 1, ord(player_white[1]) - ord("A")
                                        while row + 1 == white_row and col == white_col:
                                            new_position = input("Where do you want to move?: ").upper()
                                            new_row, new_col = int(new_position[0]) - 1, ord(new_position[1]) - ord("A") # konwert na pozycje w self.board
                                            if new_row + 3 == white_row and new_col + 2 == white_col:
                                                self.board[row * 8 + col] = "-"
                                                self.board[new_row * 8 + new_col] = "W"
                                                self.board[black_index] = "-"
                                                self.mapped_pawns_white[pawn_index_white] = [int(new_position[0]), new_position[1]]
                                                self.mapped_pawns_black.pop(pawn_index_black)
                                                beaten_pawns_black.append(pawn_index_black)
                                                self.display_board()
                                                self.white_turn = False
                                                self.move_pawn_black()
                                                break


                                    
                        elif black_col <= 6 and self.board[black_index + 7] == "W":
                            print("bialy")
                            if self.board[black_index - 7] == "-":
                                print("tutaj")
                                if white_col < 6 and self.board[white_index - 7] == "B":
                                    print("tutaj blad")
                                    if self.board[white_index - 14] == "-":
                                        print("TUTAJ BLAD")
                                        while True:
                                                player_white = input("Mandatory jump! Which pawn do you want to move?????? prawo: ").upper()
                                                pawn_index_white = self.mapped_pawns_white.index([int(player_white[0]), player_white[1]])
                                                pawn_index_black = self.mapped_pawns_black.index([pawn_black[0], pawn_black[1]])
                                                row, col = int(player_white[0]) - 1, ord(player_white[1]) - ord("A")
                                                while row + 1 == white_row and col == white_col:
                                                    new_position = input("Where do you want to move?: ").upper()
                                                    new_row, new_col = int(new_position[0]) - 1, ord(new_position[1]) - ord("A") # konwert na pozycje w self.board
                                                    if new_row + 3 == white_row and new_col - 2 == white_col:
                                                        self.board[row * 8 + col] = "-"
                                                        self.board[new_row * 8 + new_col] = "W"
                                                        self.board[black_index] = "-"
                                                        self.mapped_pawns_white[pawn_index_white] = [int(new_position[0]), new_position[1]]
                                                        self.mapped_pawns_black.pop(pawn_index_black)
                                                        beaten_pawns_black.append(pawn_index_black)
                                                        self.display_board()
                                                        self.white_turn = False
                                                        self.move_pawn_black()
                                                        break
                                                    
                                            
                else:
                    player_white = input("Which pawn do you want to move?: ").upper()
                    while any(player_white == str(pawn[0]) + str(pawn[1]) for pawn in self.mapped_pawns_white): # sprawdzenie czy element podany przez uzytkownika jest zgodny z self.mapped_pawns_white

                        row, col = int(player_white[0])-1, ord(player_white[1]) - ord('A') # Konwert na liste self.board row od 0 do 7 dlatego odejmuje 1, konwert col na kod ASCII
                        pawn_index_white = self.mapped_pawns_white.index([int(player_white[0]), player_white[1]]) # Zapisanie indexu pionka zebym mogl pozniej zmienic kordynaty w self.mapped_pawn_white po wykonaniu ruchu
                        new_position = input("Where do you want to move?: ").upper() # Zapisanie do new_position nowych kordynatow
                        # Sprawdzenie czy ruch jest zgodny z zasadami gry
                        if new_position == str(int(player_white[0]) - 1) + str(chr(ord(player_white[1]) + 1)) or new_position == str(int(player_white[0]) - 1) + str(chr(ord(player_white[1]) - 1)):
                            new_row, new_col = int(new_position[0]) -1, ord(new_position[1]) - ord('A') # Konwert na kordynaty odpowiadajace self.board
                            if self.board[new_row*8 + new_col] == "-": # Sprawdzenie czy pole jest wolne, jezeli tak 
                                self.board[row*8 + col] = "-" # Zastapienie starego miejsca "-"
                                self.board[new_row*8 + new_col] = "W" # Wstawienie w nowe miejce pionka
                                self.mapped_pawns_white[pawn_index_white] = [int(new_position[0]), new_position[1]] # Zamiana za pomoca indeksu w self.mapped_pawns_white kordynatow pionka na podane przez uzytkownika
                                self.display_board()
                                self.white_turn = False
                                self.move_pawn_black()
                                break
                            else:
                                print("This field is already occupied!")
                        else:
                            print("This field doesn't exist")
                            break


    def move_pawn_black(self):
        self.white_turn = False    
        beaten_pawns_white = []
        if self.white_turn == False:
            while True:
                for pawn_black in self.mapped_pawns_black:
                    for pawn_white in self.mapped_pawns_white:

                        black_row = pawn_black[0] # przypisanie numeru wiersza pionka czarnego do zmiennej
                        black_col = ord(pawn_black[1]) - ord("A") # przypisanie i zamiana kolumny pionka czarnego na liczbe
                        black_index = (int(black_row - 1) * 8 + black_col)

                        white_row = pawn_white[0]
                        white_col = ord(pawn_white[1]) - ord("A")
                        white_index = (int(white_row - 1) * 8 + white_col)
                        

                        
                        if white_col <= 6 and self.board[white_index - 9] == "B" and self.board[white_index + 9] == "-":
                            if black_col <= 5 and self.board[black_index + 9] == "W":
                                if self.board[black_index + 18] == "-":
                                    while True:
                                        player_black = input("Mandatory jump! Which pawn do you want to move? czarny prawo: ").upper()
                                        pawn_index_black = self.mapped_pawns_black.index([int(player_black[0]), player_black[1]]) # zapisanie indeksu w self.mapped_pawns_w
                                        pawn_index_white = self.mapped_pawns_white.index([pawn_white[0], pawn_white[1]])
                                        row, col = int(player_black[0])- 1, ord(player_black[1]) - ord('A')
                                        while row + 1 == black_row and col == black_col:
                                            new_position = input("Where do you want to move?: ").upper()
                                            new_row, new_col = int(new_position[0]) - 1, ord(new_position[1]) - ord("A") # konwert na pozycje w self.board
                                            print(f"{new_row} {new_col}")
                                            if new_row - 1 == black_row and new_col - 2 == black_col:
                                                self.board[row * 8 + col] = "-"
                                                self.board[new_row * 8 + new_col] = "B"
                                                self.board[white_index] = "-"
                                                self.mapped_pawns_black[pawn_index_black] = [int(new_position[0]), new_position[1]]
                                                self.mapped_pawns_white.pop(pawn_index_white)
                                                beaten_pawns_white.append(pawn_index_white)
                                                self.display_board()
                                                self.white_turn = True
                                                self.move_pawn_white()
                                                break

                        elif white_col >= 1 and self.board[white_index - 7] == "B":
                            print("siema")
                            if self.board[white_index + 7] == "-":
                                print("tutaj")
                                print(black_index)
                                if black_col >= 2 and self.board[black_index + 7] == "W":
                                    print("tutaj sie zatrzymuje")
                                    if self.board[black_index + 14] == "-":
                                        print("tuuu")
                                        while True:
                                            player_black = input("Mandatory jump! Which pawn do you want to move?????? czarny lewo: ").upper()
                                            pawn_index_black = self.mapped_pawns_black.index([int(player_black[0]), player_black[1]])
                                            pawn_index_white = self.mapped_pawns_white.index([pawn_white[0], pawn_white[1]])
                                            row, col = int(player_black[0]) - 1, ord(player_black[1]) - ord("A")
                                            while row + 1 == black_row and col == black_col:
                                                new_position = input("Where do you want to move?: ").upper()
                                                new_row, new_col = int(new_position[0]) - 1, ord(new_position[1]) - ord("A") # konwert na pozycje w self.board
                                                if new_row - 1 == black_row and new_col + 2 == black_col:
                                                    self.board[row * 8 + col] = "-"
                                                    self.board[new_row * 8 + new_col] = "B"
                                                    self.board[white_index] = "-"
                                                    self.mapped_pawns_black[pawn_index_black] = [int(new_position[0]), new_position[1]]
                                                    self.mapped_pawns_white.pop(pawn_index_white)
                                                    beaten_pawns_white.append(pawn_index_white)
                                                    self.display_board()
                                                    self.white_turn = True
                                                    self.move_pawn_white()
                                                    break

                else:
                    player_black = input("Which pawn do you want to move?????: ").upper()        
                    while any(player_black == str(pawn[0]) + str(pawn[1]) for pawn in self.mapped_pawns_black):
                        row, col = int(player_black[0]) -1, ord(player_black[1]) - ord('A')
                        pawn_index_black = self.mapped_pawns_black.index([int(player_black[0]), player_black[1]])
                        new_position = input("Where do you want to move?: ").upper()
                        if new_position == str(int(player_black[0]) + 1) + str(chr(ord(player_black[1]) + 1)) or new_position == str(int(player_black[0]) + 1) + str(chr(ord(player_black[1]) - 1)):
                            new_row, new_col = int(new_position[0]) -1, ord(new_position[1]) - ord('A')
                            if self.board[new_row*8 + new_col] == "-":
                                self.board[row*8 + col] = "-"
                                self.board[new_row*8 + new_col] = "B"
                                self.mapped_pawns_black[pawn_index_black] = [int(new_position[0]), new_position[1]]
                                self.display_board()
                                self.white_turn = True
                                self.move_pawn_white()
                                break
                            else:
                                print("This field is already occupied!")
                        else:
                            print("This field doesn't exist")
                            break

    

    
                        
                        

board = Board()
move = Move()
move.add_pawns_to_board()
move.map_position()
move.move_pawn_white()




